const mozjpeg = require('imagemin-mozjpeg');

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserSync: {
            dev:{
            bsFiles: {
                src : [
                    'app/css/expanded/style.min.css',  
                    'app/*.html'
                ]
            },
            options: {
                spawn:false,
                watchTask: true,
                server:  'app'
            }
            }
         },
         watch: {
            grunt: { files: ['Gruntfile.js'] },
			options:{
				spawn:false,
				livereload:true
			},
			sass:{
				files:['app/scss/**/*.scss'],
				tasks:['cssmin','sass','autoprefixer']
			},
        },
        sass:{
            options:{
                noCache:'',
                sourcemap: 'none',
                style:'expanded',
                spawn:false
            },
            dist:{files:{'app/css/style.css':'app/scss/style.scss'}},
        },
        autoprefixer: {
            options: {
                browsers: ['last 10 versions'],
                spawn:false
            },
            dist: {
                files: {
                    'app/css/style.css': 'app/css/style.css'
                }
            }
        },
        cssmin: {
            target: {
              files: [{
                expand: true,
                cwd: 'app/css/',
                src: ['*.css', '!*.min.css'],
                dest: 'app/css/expanded',
                ext: '.min.css'
              }]
            }
          },
          imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'app/images',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'app/minimages'
                }]
            }
        }
});
    //Load plugins
     require('load-grunt-tasks')(grunt);
     //Custom tasks
     //Start Server
     grunt.registerTask('startServer',['browserSync','watch']);
     //Custom Tasks
     grunt.registerTask('default',['sass','autoprefixer','cssmin','imagemin']);
};